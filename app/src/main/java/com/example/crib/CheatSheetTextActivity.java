package com.example.crib;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class CheatSheetTextActivity extends AppCompatActivity {
    Intent intent;

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat_sheet_text);
        intent = getIntent();

        webView = (WebView) findViewById(R.id.webView);

        switch (intent.getIntExtra("position", 0)) {
            case 0:
                reader("compositionPC");
                break;

            case 1:
                reader("systemUnitDevice");
                break;

            case 2:
                reader("systemUnitHousing");
                break;

            case 3:
                reader("motherboard");
                break;

            case 4:
                reader("processorAndCoolingSystem");
                break;

            case 5:
                reader("ramModule");
                break;

            case 6:
                reader("hardDrive");
                break;

            case 7:
                reader("videoCard");
                break;

            case 8:
                reader("theOpticalDiscDrive");
                break;

            case 9:
                reader("floppyDriveAndCardReaders");
                break;

            case 10:
                reader("audioAdapterModemAndLANController");
                break;

            case 11:
                reader("powerSupply");
                break;
        }
    }

    public void reader(String name) {
        webView.loadUrl("file:///android_asset/" + name + ".html");
    }
}
